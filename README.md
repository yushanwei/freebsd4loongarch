20241124:patch from stable14<br>
20241122:upgrade elftoolchain to Ver4049<br>
20241119<br>
20241024,llvm19<br>
20241009<br>
2024048 WITH_EFI<br>
20240407 FreeBSD15升级llvm到18.1.3了，重新制作patch<br>
20240328 升级llvm到18,来自[dimitryandric](https://github.com/dimitryandric),make world竟然通过。肯定不能正常使用。<br>
20240305 main分支make toolchain 通过<br>
20240202 头文件和csu持续更新中<br>
# FreeBSD for LoongArch<br>
在FB的main、stable/14、releng/14.0三个分支上，按文件名顺序打上补丁，make  kernel-toolchain能通过。<br>
完全代码在[github.com](https://github.com/yushanwei/freebsd-src)<br>
HOST相关的更新：<br>
libmagic<br>
elftoolchain:libelf,libelftc,libpe<br>
sys/contrib/edk2<br>

src-env.conf<br>
loongarch.sh<br>

#### 使用说明
1.  setenv TARGET loongarch<br>
2.  setenv TARGET_ARCH loongarch64<br>
3.  make  kernel-toolchain<br>
